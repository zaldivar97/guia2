/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.tpi2019.guia02.facades;

import java.util.List;
import javax.ejb.Local;
import sv.edu.uesocc.tpi2019.guia02.entities.Articulo;

/**
 *
 * @author irvin
 */
@Local
public interface ArticuloFacadeLocal extends AbstractInterface<Articulo>{   
    public List<Articulo> findByIdEquipo(int idEquipo);
}
