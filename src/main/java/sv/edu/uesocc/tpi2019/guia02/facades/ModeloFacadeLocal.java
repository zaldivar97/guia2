/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.tpi2019.guia02.facades;

import javax.ejb.Local;
import sv.edu.uesocc.tpi2019.guia02.entities.Modelo;

/**
 *
 * @author irvin
 */
@Local
public interface ModeloFacadeLocal extends AbstractInterface<Modelo>{

}
