/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.tpi2019.guia02.facades;

import javax.ejb.Stateless;

/**
 *
 * @author zaldivar
 */
        @Stateless
class SaludoFacade {

    String saludar(String name) {
return new StringBuilder().append("Hola").append(" ").append(name).toString();
        }
    
}
