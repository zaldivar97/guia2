/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.tpi2019.guia02.facades;

import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Rule;
import sv.edu.uesocc.tpi2019.guia02.entities.Articulo;

/**
 *
 * @author zaldivar
 */
public class ArticuloFacadeIT {

    public ArticuloFacadeIT() {
    }
    ArticuloFacade art = new ArticuloFacade();
    @Rule
    public EntityManagerProvider emp = EntityManagerProvider.getInstance("mantenimientoPU-test",art);
    

    @BeforeClass
    public static void setUpClass() {

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {

    }

    public AbstractFacade facade(){
        return emp.getFacade();
    }
    
    @Test
    public void testDelete() {
        Articulo articulo1 = new Articulo(1);
        Articulo articulo2 = new Articulo(2);
        facade().getEntityManager().getTransaction().begin();
        facade().crear(articulo1);
        facade().crear(articulo2);
        facade().remove(new Articulo(1));
        assertEquals(articulo2, facade().findById(2));
        fail("testDelete");
    }

    @Test
    public void testCreate() {
        Articulo articulo = new Articulo(1);
        facade().getEntityManager().getTransaction().begin();
        facade().crear(articulo);
        List list = facade().findAll();
        assertEquals(1, list.size());
                fail("testCreate");

    }

    @After
    public void tearDown() {

    }

}
